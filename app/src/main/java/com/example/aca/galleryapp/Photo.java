package com.example.aca.galleryapp;

public class Photo {

    private String name;
    private String path;
    private String folderName;

    public Photo(String name, String path, String folderName) {
        this.name = name;
        this.path = path;
        this.folderName = folderName;
    }


    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getFolderName() {
        return folderName;
    }
}
