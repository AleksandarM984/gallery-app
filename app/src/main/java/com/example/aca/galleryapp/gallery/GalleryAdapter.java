package com.example.aca.galleryapp.gallery;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aca.galleryapp.Photo;
import com.example.aca.galleryapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    public interface OnClickPhotoListener {
        void onPhotoClick(Photo photo, ImageView imageView);
    }

    private Context context;
    private List<Photo> photos;
    private OnClickPhotoListener onClickPhotoListener;


    public GalleryAdapter(List<Photo> photos, Context context, OnClickPhotoListener onClickPhotoListener) {
        this.photos = photos;
        this.context = context;
        this.onClickPhotoListener = onClickPhotoListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {
        Photo photo = photos.get(position);
        viewHolder.nameTV.setText(photo.getName());
        Glide.with(context).load("file://".concat(photo.getPath()))
                .thumbnail(0.1f)
                .into(viewHolder.photoIV);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.photoIV)
        ImageView photoIV;
        @BindView(R.id.nameTV)
        TextView nameTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.photoIV)
        public void onItemClick() {
            onClickPhotoListener.onPhotoClick(photos.get(getAdapterPosition()), photoIV);
        }
    }
}
