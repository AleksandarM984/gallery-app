package com.example.aca.galleryapp.gallery;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.example.aca.galleryapp.Photo;

import java.util.ArrayList;
import java.util.List;

public class GalleryManager {

    private List<Photo> allPhotos = new ArrayList<>();
    private Callback callback;
    private Uri uri;
    private Cursor cursor;
    private Context context;
    private int columnIndexData;
    private int columnIndexFolderName;
    private int columnIndexDisplayName;
    String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME,  MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Thumbnails.DATA};

    final String orderBy = MediaStore.Images.Media.DATE_TAKEN;

    public GalleryManager(Uri uri, Context context, Callback callback) {
        this.uri = uri;
        this.context = context;
        this.cursor = context.getContentResolver().query(uri, projection, null, null, orderBy + " DESC");
        this.callback = callback;
        columnIndexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        columnIndexFolderName = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
        columnIndexDisplayName = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
    }

    public void loadPhotos() {
        String absolutePathPhoto;
        String photoName;
        String photoFolder;

        while (cursor.moveToNext()) {
            absolutePathPhoto = cursor.getString(columnIndexData);
            photoName = cursor.getString(columnIndexDisplayName);
            photoFolder = cursor.getString(columnIndexFolderName);
            Log.d("Column", absolutePathPhoto);
            Log.d("Folder", photoName);
            Log.d("Display name", photoFolder);
            Photo photo = new Photo(photoName, absolutePathPhoto, photoFolder);
            allPhotos.add(photo);

        }
        callback.onPhotosLoadedFromGallery(allPhotos);
    }

    interface Callback{
        void onPhotosLoadedFromGallery(List<Photo> photos);
    }

}
